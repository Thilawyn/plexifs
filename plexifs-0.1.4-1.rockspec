-- rockspec_format = "3.0"

package = "plexifs"
version = "0.1.4-1"

source = {
    url = "git+https://gitlab.com/Thilawyn/plexifs.git",
    tag = "v0.1.4",
}

description = {
    summary    = "Filesystem abstraction library using Plexiclass",
    detailed   = "Filesystem abstraction library using Plexiclass",
    license    = "MIT",
    homepage   = "https://gitlab.com/Thilawyn/plexifs",
    -- issues_url = "https://gitlab.com/Thilawyn/plexifs/-/issues",
    maintainer = "Julien Valverdé <julien.valverde@mailo.com>",
    -- labels     = {"moonscript", "oop", "plexiclass", "plexifs", "filesystem"},
}

supported_platforms = {"!windows"}

dependencies = {
    "lua >= 5.1",
    "moonscript",
    "plexiclass",
    "lua-path",
    "luafilesystem",
    "lua-cjson",
}

-- build_dependencies = {
--     "moonscript",
-- }

build = {
    type = "make",
    build_variables = {
        CFLAGS     = "$(CFLAGS)",
        LIBFLAG    = "$(LIBFLAG)",
        -- LUA_LIBDIR = "$(LUA_LIBDIR)",
        LUA_BINDIR = "$(LUA_BINDIR)",
        LUA_INCDIR = "$(LUA_INCDIR)",
        LUA        = "$(LUA)",
    },
    install_variables = {
        INST_PREFIX  = "$(PREFIX)",
        INST_BINDIR  = "$(BINDIR)",
        INST_LIBDIR  = "$(LIBDIR)",
        INST_LUADIR  = "$(LUADIR)",
        INST_CONFDIR = "$(CONFDIR)",
    },
}
