import RawFile from require "plexifs.RawFile"


Video: RawFile\extend "plexifs.Video", =>

    --
    -- File type
    --

    cls mime_type: "video/*"
