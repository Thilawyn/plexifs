{
    name:          "Thilawyn/plexifs"
    version:       "0.1.4"
    description:   "Filesystem abstraction library using Plexiclass"
    tags:          {"moonscript", "oop", "filesystem"}
    license:       "MIT"
    author:
        name:   "Julien Valverdé"
        email:  "julien.valverde@mailo.com"
    homepage:      "https://gitlab.com/Thilawyn/plexifs"
    dependencies:  {"Thilawyn/plexiclass", "lduboeuf/cjson"}
    files:         {"**.lua"}
}
