export _

is_luvit = pcall require, "luvi"

import List from require "plexiclass"
import File from require "plexifs.File"

local fs, lfs
if is_luvit
    fs  = require "fs"
else
    lfs = require "lfs"

import os from _G


Directory: File\extend "plexifs.Directory", =>

    --
    -- File type
    --

    cls mime_type: "inode/directory"


    --
    -- Object creation
    --

    cls __call: (file_path, values) =>
        @create_from_path file_path, values


    --
    -- Default directories
    --

    cls get home: => @ os.getenv "HOME"


    --
    -- Directory content
    --

    if is_luvit
        --- Returns a table containing the name of the files in the directory.
        obj get names: =>
            @assert_readable!
            List\from_data fs.readdirSync @path

        --- Returns a table containing the name of the files in the directory (async version).
        obj names_async: (f) =>
            fs.readdir @path, (err, names) ->
                f names and List\from_data(names) or err

        --- Creates the directory.
        obj create: =>
            fs.mkdirSync @path

        --- Creates the directory (async version).
        obj create_async: (f) =>
            fs.mkdir @path, _, f

    else
        --- Returns a table containing the name of the files in the directory.
        obj get names: =>
            @assert_readable!

            l = List!
            for name in lfs.dir @path
                if name != "." and name != ".."
                    l\insert_last name

            l

        --- Creates the directory.
        obj create: =>
            lfs.mkdir @path

    --- Returns the child file of the given name.
    obj child: (name, values) =>
        @child_as name, File, values

    --- Returns the child file of the given name, as an instance of the given class.
    obj child_as: (name, cls, values) =>
        cls @path .. "/" .. name, values

    --- Returns a List containing all the files and directories of the directory.
    obj get children: =>
        file_paths = @names\map((k, v) -> @path .. "/" .. v)

        -- Get the mime type of all the files in one go (efficient optimization)
        mime_types = @@read_mime_types file_paths

        file_paths\map (k, v) ->
            File v, _, mime_types[ k ]

    --- Returns a List containing all the files of the directory, and all the files of the subdirectories, etc...
    obj get children_flattened: =>
        file_paths = @names\map((k, v) -> @path .. "/" .. v)

        -- Get the mime type of all the files in one go (efficient optimization)
        mime_types = @@read_mime_types file_paths

        file_paths\map (k, v) ->
            File v, _, mime_types[ k ]
