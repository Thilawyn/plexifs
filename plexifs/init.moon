-- Luvit compatibility
if pcall require, "luvi"
    -- Preload the scripts with Luvit's loader
    with package.loaded
        ["plexifs.util"]      = require "./util"

        ["plexifs.File"]      = require "./File"
        ["plexifs.Directory"] = require "./Directory"
        ["plexifs.RawFile"]   = require "./RawFile"
        ["plexifs.Image"]     = require "./Image"
        ["plexifs.Video"]     = require "./Video"
        ["plexifs.JsonFile"]  = require "./JsonFile"


{
    File:       require("plexifs.File").File

    Directory:  require("plexifs.Directory").Directory
    RawFile:    require("plexifs.RawFile").RawFile
    Image:      require("plexifs.Image").Image
    Video:      require("plexifs.Video").Video
    JsonFile:   require("plexifs.JsonFile").JsonFile
}
