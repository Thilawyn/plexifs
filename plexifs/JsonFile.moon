is_luvit = pcall require, "luvi"

import Object  from require "plexiclass"
import RawFile from require "plexifs.RawFile"

cjson = require "cjson"

import pcall from _G


JsonFile: RawFile\extend "plexifs.JsonFile", =>

    --
    -- File type
    --

    cls mime_type: "application/json"


    --
    -- File content
    --

    obj get parsed_content: => @read_parsed!
    obj set parsed_content: (k, v) => @write_encoded v

    --- Parses the content of the JSON file and returns the resulting JSON data.
    -- Throws an error if the file cannot be opened.
    obj read_parsed: =>
        content = @read!
        cjson.decode content

    obj read_parsed_as: (structure) =>
        Object\parse_as @read_parsed!, structure

    obj write_encoded: (data) =>
        -- TODO: implement encoding in Plexiclass
        @write cjson.encode data

    if is_luvit
        --- Parses the content of the JSON file and returns the resulting JSON data (asynchronous version).
        obj read_parsed_async: (f) =>
            @read_async (content, err) ->
                if not content
                    return f nil, err

                ok, res = pcall cjson.decode, content
                f ok and res or nil, not ok and res or nil

        obj read_parsed_as_async: (structure, f) =>
            @read_parsed_async (parsed_content, err) ->
                if not parsed_content
                    return f nil, err

                Object\parse_as parsed_content, structure

        obj write_encoded_async: (data, f) =>
            @write_async cjson.encode(data), f
