-- Luvit compatibility
if pcall require, "luvi"
    -- Preload the scripts with Luvit's loader
    with package.loaded
        ["plexifs"] = require "./init"


for k, v in pairs require "plexifs"
    _G[ k ] = v
