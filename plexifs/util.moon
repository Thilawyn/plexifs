import assert, io from _G


{
    --- Returns the output of a system command.
    capture_output: (cmd, raw = false) ->
        f = assert io.popen cmd, "r"
        s = assert f\read "*a"
        f\close!

        return s if raw

        s = s\gsub("^%s+", "")\gsub("%s+$", "")\gsub("[\n\r]+", "")
        s
}
