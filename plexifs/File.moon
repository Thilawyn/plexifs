is_luvit = pcall require, "luvi"

import Object, List   from require "plexiclass"
import capture_output from require "plexifs.util"

path = require "path"

local fs, lfs
if is_luvit
    fs  = require "fs"
else
    lfs = require "lfs"

import assert from _G


File: Object\extend "plexifs.File", =>

    --
    -- Util
    --

    --- Normalizes the given path.
    cls normalize_path: do
        if is_luvit
            (file_path) => path.resolve file_path
        else
            (file_path) => path.fullpath file_path

    --- Basename of the given path.
    -- cls basename: (file_path) =>
    --     path.basename file_path

    --- Dirname of the given path.
    -- cls dirname: (file_path) =>
    --     path.dirname file_path

    --- Extension of the given path.
    -- cls extname: (file_path) =>
    --     is_luvit and path.extname(file_path) or path.extension(file_path)

    --- Returns the mime type of the file at the given path.
    cls read_mime_type: (file_path) =>
        -- TODO: detect OS & return nil if not mime returned
        capture_output "/usr/bin/file -b --mime-type \"" .. file_path .. "\"", true

    --- Returns the mime type of the files at the given path.
    cls read_mime_types: (file_paths) =>
        -- TODO: detect OS & return nil if not mime returned
        List\split (capture_output "/usr/bin/file -b --mime-type " .. file_paths\map((k, v) -> "\"" .. v .. "\"")\join(" "), true), "\n"


    --
    -- File type
    --

    cls defer Directory: => require("plexifs.Directory").Directory

    cls defer hooks: => List {
        @Directory
        require("plexifs.Image").Image
        require("plexifs.Video").Video
        require("plexifs.JsonFile").JsonFile
        require("plexifs.RawFile").RawFile
    }

    --- Returns whether or not the file located at the given path matches the type of the file class.
    cls matches_file_type: (file_path, mime_type) =>
        mime_type\match(@mime_type) != nil


    --
    -- File properties
    --

    obj pro path:
        type: "string"

    obj defer name: =>
        path.basename @path

    obj defer extension: do
        if is_luvit
            => path.extname @path
        else
            => path.extension @path

    obj defer parent: =>
        if @path != "/"
            @@.Directory\create_from_path path.dirname @path

    --- Returns the file's mime type.
    obj get mime_type: =>
        @@read_mime_type @path


    --
    -- Object creation
    --

    cls __call: (file_path, values, mime_type) =>
        -- Normalize the path
        file_path = @normalize_path file_path

        -- Detect the file type and choose the correct subclass
        mime_type or= @read_mime_type(file_path) or ""

        cls = @hooks\each_until (_, cls) ->
            cls\matches_file_type(file_path, mime_type) and cls or nil

        -- Create the object
        cls\create_from_path file_path, values

    cls create_from_path: (file_path, values = {}) =>
        values.path = @normalize_path file_path
        @create values


    if is_luvit
        --- Indicates whether or not the file exists on disk.
        obj get exists: =>
            fs.existsSync(@path) and @@matches_file_type @path, @mime_type

        obj exists_async: (f) =>
            fs.exists @path, (exists) ->
                f exists and @@matches_file_type @path, @mime_type

        --- Indicates whether or not the file is readable by the current user.
        obj get readable: =>
            -- TODO
            true

        obj readable_async: (f) =>
            -- TODO
            f true

        --- Indicates whether or not the file is writable by the current user.
        obj get writable: =>
            -- TODO
            true

        obj writable_async: (f) =>
            -- TODO
            f true

        --- Returns a table containing the statistics of the file, or nil if it doesn't exist or is unreadable.
        obj get stats: =>
            fs.lstatSync @path

        obj stats_async: (f) =>
            fs.lstat @path, (_, stats) ->
                f stats

        obj get last_access: =>
            if stats = @stats
                stats.atime.sec

        obj last_access_async: (f) =>
            @stats_async (stats) ->
                f stats and stats.atime.sec

        obj get last_modification: =>
            if stats = @stats
                stats.mtime.sec

        obj last_modification_async: (f) =>
            @stats_async (stats) ->
                f stats and stats.mtime.sec

        obj get last_change: =>
            if stats = @stats
                stats.ctime.sec

        obj last_change_async: (f) =>
            @stats_async (stats) ->
                f stats and stats.ctime.sec

    else
        --- Indicates whether or not the file exists on disk.
        obj get exists: =>
            lfs.attributes(@path) != nil and @@matches_file_type @path, @mime_type

        --- Indicates whether or not the file is readable by the current user.
        obj readable: =>
            -- TODO
            true

        --- Indicates whether or not the file is writable by the current user.
        obj writable: =>
            -- TODO
            true

        obj get last_access: =>
            lfs.attributes @path, "access"

        obj get last_modification: =>
            lfs.attributes @path, "modification"

        obj get last_change: =>
            lfs.attributes @path, "change"


    --
    -- Serialization
    --

    cls from_data: (data) =>
        @ data.path, data

    obj to_data: =>
        with parent @
            .name      = @name
            .extension = nil
            .parent    = nil
            .mime_type = nil


    --
    -- Assertions
    --

    obj assert_exists: =>
        assert @exists, @path .. " does not exist on the filesystem."

    obj assert_readable: =>
        assert @readable, @path .. " is not readable."

    obj assert_writable: =>
        assert @writable, @path .. " is not writable."


    --
    -- Metamethods
    --

    obj __tostring: =>
        @@name .. ": " .. @path
