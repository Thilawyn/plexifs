is_luvit = pcall require, "luvi"

import File from require "plexifs.File"

fs = if is_luvit
    require "fs"

import
    assert
    io
from _G


RawFile: File\extend "plexifs.RawFile", =>

    --
    -- File type
    --

    cls mime_type: ""


    --
    -- Object creation
    --

    cls __call: (file_path, values) =>
        @create_from_path file_path, values


    --
    -- File content
    --

    obj get content: => @read!
    obj set content: (k, v) => @write v

    if is_luvit
        obj read: =>
            assert fs.readFileSync @path

        obj read_async: (f) =>
            fs.readFile @path, (err, content) ->
                f content, err

        obj write: (data) =>
            fs.writeFileSync @path, data

        obj write_async: (data, f) =>
            fs.writeFile @path, data, f

        obj append: (data) =>
            fs.appendFileSync @path, data

        obj append_async: (data, f) =>
            fs.appendFile @path, data, f

    else
        obj open: (mode = "r") =>
            assert io.open @path, mode

        obj read: =>
            handle = @open "r"
            c = handle\read "*all"
            handle\close!

            c

        obj write: (data) =>
            handle = @open "w"
            c = handle\write data
            handle\close!

            c

        obj append: (data) =>
            handle = @open "a"
            c = handle\write data
            handle\close!

            c
