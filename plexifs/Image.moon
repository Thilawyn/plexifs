import RawFile from require "plexifs.RawFile"


Image: RawFile\extend "plexifs.Image", =>

    --
    -- File type
    --

    cls mime_type: "image/*"
