import RawFile, JsonFile from require "plexifs"
import p              from require "moon"


MyFile = RawFile\extend "tests.MyFile", =>

    obj pro a_prop:
        type: "string"


data = JsonFile("tests/data.json")\to_data!
p data
data.a_prop = "aya"

print MyFile\from_data data
