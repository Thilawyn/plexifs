import File, JsonFile    from require "plexifs"
import Object, Set, List from require "plexiclass"
import p                 from require "moon"


ImageModel = Object\extend "tests.ImageModel", =>

    obj pro id:
        type: "number"
    obj pro src:
        type: "string"

    obj __tostring: =>
        @@name .. "(" .. @id .. ", " .. @src .. ")"


json_file = File "tests/data.json"
parsed_data = json_file\read_parsed_as
    class: Set
    keys:
        images:
            class: List
            all:
                class: ImageModel

print parsed_data

new_json_file = JsonFile "tests/new_data.json"
new_json_file.parsed_content = parsed_data\to_data!
